package config

type ServerConfig struct {
	Port        int           `mapstructure:"port"`
	ChatBotInfo ChatBotConfig `mapstructure:"chatbot-api"`
}
type ChatBotConfig struct {
	LaunchList string      `mapstructure:"launchList"`
	GroupInfo  GroupConfig `mapstructure:"group01"`
}

type GroupConfig struct {
	GroupName      string `mapstructure:"groupName"`
	GroupId        string `mapstructure:"groupId"`
	Cookie         string `mapstructure:"cookie"`
	OpenAiKey      string `mapstructure:"openAiKey"`
	CronExpression string `mapstructure:"cronExpression"`
	Silenced       bool   `mapstructure:"silenced"`
}
