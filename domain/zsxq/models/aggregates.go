package models

type UnAnsweredQuestionsAggregates struct {
	Succeeded bool
	RespData  RespData `json:"resp_data"`
}

type RespData struct {
	Topics []Topic `json:"topics"`
}

type Topic struct {
	TopicId       int          `json:"topic_id"`
	Group         Group        `json:"group"`
	TopicType     string       `json:"type"`
	Question      Question     `json:"question"`
	Answered      bool         `json:"answered"`
	LikesCount    int          `json:"likes_count"`
	RewardsCount  int          `json:"rewards_count"`
	CommentsCount int          `json:"comments_count"`
	ReadingCount  int          `json:"reading_count"`
	ReadersCount  int          `json:"readers_count"`
	Digested      bool         `json:"digested"`
	Sticky        bool         `json:"sticky"`
	CreateTime    string       `json:"create_time"`
	UserSpecific  UserSpecific `json:"user_specific"`
}

type Group struct {
	GroupId   int    `json:"group_id"`
	Name      string `json:"name"`
	GroupType string `json:"type"`
}

type Question struct {
	Owner         Owner       `json:"owner"`
	Questionee    Questionee  `json:"questionee"`
	Text          string      `json:"text"`
	Expired       bool        `json:"expired"`
	Anonymous     bool        `json:"anonymous"`
	OwnerDetail   OwnerDetail `json:"owner_detail"`
	OwnerLocation string      `json:"owner_location"`
}

type Owner struct {
	UserId    int    `json:"user_id"`
	Name      string `json:"name"`
	AvatarUrl string `json:"avatar_url"`
	Location  string `json:"location"`
}

type Questionee struct {
	UserId      int    `json:"user_id"`
	Name        string `json:"name"`
	AvatarUrl   string `json:"avatar_url"`
	Location    string `json:"location"`
	Description string `json:"description"`
}

type OwnerDetail struct {
	QuestionsCount int    `json:"questions_count"`
	JoinTime       string `json:"join_time"`
}

type UserSpecific struct {
	Liked      bool `json:"liked"`
	Subscribed bool `json:"subscribed"`
}
