package models

type AnswerReq struct {
	ReqData ReqData `json:"req_data"`
}

type ReqData struct {
	Text     string   `json:"text"`
	ImageIds []string `json:"image_ids"`
	Silenced bool     `json:"silenced"`
}

type AnswerRes struct {
	Succeeded bool `json:"succeeded"`
}
