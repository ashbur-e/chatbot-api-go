package api

import (
	"chatbot-api-go/global"
	"chatbot-api-go/initialize"
	"fmt"
	"testing"
)

func init() {
	initialize.InitConfig()
}

func TestQueryUnAnsweredQuestionsTopicId(t *testing.T) {

	questions, err := QueryUnAnsweredQuestionsTopicId(global.ServerConfig.ChatBotInfo.GroupInfo.GroupId, global.ServerConfig.ChatBotInfo.GroupInfo.Cookie)
	if err != nil {
		panic(err)
	}
	fmt.Println(questions.Succeeded)
	fmt.Println(questions.RespData.Topics)
}

func TestAnswer(t *testing.T) {
	Answer(global.ServerConfig.ChatBotInfo.GroupInfo.GroupId, global.ServerConfig.ChatBotInfo.GroupInfo.Cookie, 214884824112481, "好的", false)
}
