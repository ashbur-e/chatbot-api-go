package main

import (
	"chatbot-api-go/global"
	"chatbot-api-go/initialize"
	"fmt"
	"go.uber.org/zap"
)

func main() {
	// 0. 初始化logger
	initialize.InitLogger()
	// 1. 初始化配置信息
	initialize.InitConfig()
	// 2. 初始化校验翻译
	_ = initialize.InitTrans("zh")
	// 3. 初始化Routers
	Router := initialize.Routers()
	// 4. 初始化定时任务
	initialize.InitTask()

	zap.S().Infof("服务启动ing，端口：%d", global.ServerConfig.Port)
	if err := Router.Run(fmt.Sprintf(":%d", global.ServerConfig.Port)); err != nil {
		zap.S().Panic("启动失败！", err.Error())
	}

}
