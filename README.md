# 《ChatGPT AI 问答助手》开源免费项目

## Java语言版本
- [小傅哥知识星球ChatApi](https://github.com/fuzhengwei/chatbot-api.git)

## 1. 项目介绍
**ChatGPT AI 问答助手** 是参考京东大佬小傅哥知识星球ChatApi项目复刻而来的，Go语言版本实现；项目简单精炼，涉及涵盖爬虫接口、ChatGPT API对接、DDD架构设计

作为Go语言入门级项目以供使用以及参考。

## 2. 项目内容
- 定时爬取知识星球问题列表接口
- 调用ChatGpt API实现AI智能回答
- 调用知识星球接口实现自动答复

## 3. 使用Go Package
- Web框架-[gin](https://github.com/gin-gonic/gin.git)
- 日志框架-[zap](https://github.com/uber-go/zap.git)
- 定时任务框架-[cron](https://github.com/robfig/cron.git)

## 4. 使用方法
正月十五得过节去了，有问题请联系作者微信：L1763077056